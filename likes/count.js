const fs = require('fs')
const moment = require('moment')

function execute() {
    let count = 0
    const likes = JSON.parse(fs.readFileSync("./likes.json"))
    // console.log(JSON.stringify(likes))
    let result = likes.media_likes.filter(record => moment(record[0]).year() == 2019)
    console.log(result.length)
}

execute()