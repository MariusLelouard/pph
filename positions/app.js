const fs = require('fs')
const moment = require('moment')
// const csvWriterFactory = require('csv-writer').createArrayCsvWriter
const csv = require('fast-csv')

function delta(data1, data2) {
    const R = 6371; // km (change this constant to get miles)
	const dLat = (data1.latitude-data2.latitude) * Math.PI / 180;
	const dLon = (data1.longitude-data2.longitude) * Math.PI / 180;
	const a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(data1.latitude * Math.PI / 180 ) * Math.cos(data2.latitude * Math.PI / 180 ) *
		Math.sin(dLon/2) * Math.sin(dLon/2);
	const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    const d = R * c;
    return d*1000
}

async function execute() {
    const positions = JSON.parse(fs.readFileSync('positions/positions.json', 'utf8'))
    // const csvWriter = csvWriterFactory({
    //     path:'positions.csv',
    //     header: [
    //         {id: 'longitude', title: 'Longitude'},
    //         {id: 'latitude', title: 'Latitude'},
    //         {id: 'date', title: 'Date'}
    //       ]
    // })
    const csvStream = csv.format({headers: true})
    const ws = fs.createWriteStream("out.csv");
    csvStream.pipe(ws).on('end', process.exit)

    var prevData = {
        longitude: positions.locations[0].longitudeE7/10000000,
        latitude: positions.locations[0].latitudeE7/10000000,
        date: moment(parseInt(positions.locations[0].timestampMs)).format()
    }
    positions.locations.slice(1).forEach(location => {
        var data =
        {
            longitude: location.longitudeE7/10000000,
            latitude: location.latitudeE7/10000000,
            weight: 1,
            date: moment(parseInt(location.timestampMs)).format()
        }
        if (delta(data, prevData) > 10) {
            csvStream.write(data)
            prevData = data
        }
    });
    csvStream.end()

}
execute();