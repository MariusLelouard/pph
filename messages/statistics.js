const fs = require('fs')
const wf = require('word-freq')
const natural = require('natural')

const output = "freq.json"
const input = "rawMe.txt"
function execute() {
    const data = fs.readFileSync(input,"utf8")
    console.log("loaded")

    let globalFreq = {}

    let words = natural.PorterStemmerFr.tokenizeAndStem(data)
    const TfIdf = natural.TfIdf
    let tfidf = new TfIdf()
    tfidf.addDocument(words)
    const result = tfidf.listTerms(0).map(token => {
        return {"term":token.term, "tf":token.tf}
    }).sort((a, b) => {
        return b.tf - a.tf
    }).slice(0,50)
    console.log(JSON.stringify(result))

    // data.messages.forEach(message => {
    //     console.log(message)
    //     let freq
    //     try {
    //         freq = wf.freq(message)
    //     } catch (_err) {
    //         // message = message.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '')
    //         // freq = wf.freq(message)
    //     }  {
    //         if(freq) {
    //             Object.keys(freq).forEach(word => {
    //                 if (globalFreq[word]) {
    //                     globalFreq[word] += freq[word]
    //                 } else {
    //                     globalFreq[word] = 1
    //                 }
    //             })
    //             console.log(JSON.stringify(freq))
    //         }
    //     }
    // });
    // console.log(JSON.stringify(globalFreq))
}

execute()