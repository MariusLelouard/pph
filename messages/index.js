function ShowLoader() {
  var overlay = jQuery(
    `<div id="loading-overlay" class="d-flex flex-column justify-content-center" style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 1); z-index: 10000;">
    <span class="text-center text-light mb-3"> Loading...</span>
    <div class="spinner-grow text-light mx-auto" role="status">
    <span class="sr-only">Loading...</span>
  </div>
    </div>`
  );
  overlay.appendTo(document.body);
}
function HideLoader() {
  $("#loading-overlay").remove();
}



class MessageGenerator {
  constructor(max = 10) {
    this.realCount = 0;
    this.generatedCount = 0;
    this.max = max;
    this.messages = $.getJSON("./out.json");
    this.messages.then(()=>HideLoader())
    this.generatedMessages = $.getJSON("./generated.json");
    this.discussion = [];
    this.template = $("#message-template").contents().clone();
  }

  animate(message) {
    message.appendTo("#conversation");
    $("#conversation").scrollTop($("#conversation").get(0).scrollHeight);
    this.discussion.push(message);
    this.discussion.forEach((message, index, array) => {
      message.css("opacity", Math.pow(0.8, Math.abs(array.length - 3 - index)));
      // const coeff = 1-(Math.exp(Math.abs(array.length-3-index)/(array.length-3))-1)/(2*Math.E)
      if (index == array.length - 3) {
        message.css("transform", `scale(1.02)`);
      } else {
        message.css("transform", `scale(0.98)`);
      }
    });
  }

  async generateMessage() {
    const messages = await this.messages;
    const generatedMessages = await this.generatedMessages;
    const random = Math.floor(Math.random() * 10);
    let text = "";

    if (random < 5) {
      this.realCount++;
      const randomPos = Math.floor(Math.random() * messages.messages.length);
      text = messages.messages[randomPos];
    } else {
      this.generatedCount++;
      const randomPos = Math.floor(
        Math.random() * generatedMessages.messages.length
      );
      text = generatedMessages.messages[randomPos];
    }
    var newMessage = this.template.clone();
    newMessage.find(".card-body").text(`${text}`);

    this.animate(newMessage);
    if (this.discussion.length > this.max) {
      const message = this.discussion.shift();
      message.css("opacity", 0);
      setTimeout(() => {
        message.remove();
      }, 500);
    }

    // // this.discussion.push(text)
    // this.discussion.forEach((message)=>{

    // })

    // newMessage.appendTo("#conversation")
    // newMessage.removeClass("card-empty")
    // this.discussion.push(newMessage)

    // this.discussion.forEach((message,index,array)=>{
    //     message.css("opacity", 1*Math.pow(0.8,array.length-index-1))
    // })
  }
}
ShowLoader();
const messageGenerator = new MessageGenerator(10);
setInterval(() => {
  messageGenerator.generateMessage();
}, 500);
