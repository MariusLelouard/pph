const fs = require('fs')
const cheerio = require('cheerio')

const output = "rawMe.txt"
const dataPath = "/mnt/c/Users/mariu/Downloads/facebook-mariuslelouard/messages/inbox"
function execute() {
    
    fs.readdir(dataPath, function (err, dirs) {
        var stream = fs.createWriteStream(output, {flags:'w'});
        // stream.write("{\r\n\"messages\":[\r\n")
        //handling error
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        } 
        //listing all files using forEach
        dirs.forEach(function (dir) {
            // console.log(`-----${dir}`)
            fs.readdir(`${dataPath}/${dir}`, function (err, files) {
                if (err) {
                    return console.log('Unable to scan directory: ' + err);
                }
                files.filter(function(file) {
                    if (file.includes("message")) {
                        return true
                    }
                }).forEach(function(file,index) {
                    // console.log(`----------${file}`)
                    const $ = cheerio.load(fs.readFileSync(`${dataPath}/${dir}/${file}`));
                    $("body > div > div > div > div._3a_u > div._4t5n > div > div._3-96._2let > div > div:nth-child(2)")
                    .filter((_i, message)=>{
                        try{
                            const sender = message.parent.parent.previousSibling.firstChild.data
                            return sender == "Marius Lelouard"
                        } catch (_err) {
                            return false
                        }
                    })
                    .each((_i, el) => {
                        try{
                            // const regex = /["\\\t]/gi
                            let text = el.firstChild.data//.replace(regex,"$")
                            // stream.write(`"${text}",\r\n`)
                            stream.write(text+" ")
                        } catch (err) {}
                    })
                })
            })
        });
    });
}

execute()